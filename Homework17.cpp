﻿#include <iostream>
#include <cmath>
#include <vector>
#include <cassert>

using std::cout;
using std::endl;

/* Class for Homework 17*/
class MyClass 
{
private:
    std::string Secret1{ "Just a string." };
    int Secret2{ 1 };
public:
    void ShowSecrets() 
    {
        std::cout << Secret1 << ' ' << Secret2 << std::endl;
    }
};

class Vector
{
private:
    double X;
    double Y;
    double Z;
public:
    Vector() : X(0), Y(0), Z(0) 
    { }
    Vector(double _X, double _Y, double _Z)
        : X(_X), Y(_Y), Z(_Z) 
    { }
    double Module() const
    {
        return sqrt(pow(X, 2) + pow(Y, 2) + pow(Z, 2));
    }
};

/* Stack for Homework 18 */
/*template<typename T>
class Stack
{
private:
    std::vector<T> Data;
public:
    T& top()
    {
        if (!Data.size()) {
            throw std::runtime_error("Attempt to access for empty stack.");
        }
        return Data.back();
    }
    void push(const T& _Item)
    {
        Data.push_back(_Item);
    }
    T& pop() 
    {
        T& Item = this->top();
        Data.pop_back();
        return Item;
    }
    int size() const
    {
        return Data.size();
    }
};*/

template<typename T>
class Stack
{
private:
    T* m_Data      { nullptr };
    int m_Top      { -1 };
    int m_Size     { 0 };
    int m_Capacity { 0 };
private:
    void Relocate() 
    {
        if (m_Capacity == 0) {
            m_Capacity = 2;
            m_Data = new T[m_Capacity];
        }
        else {
            m_Capacity *= 2;
            T* Buf = new T[m_Capacity];
            memcpy(Buf, m_Data, sizeof(T)*m_Size);
            delete[] m_Data;
            m_Data = Buf;
        }
    }
public:
    Stack()
    { }
    explicit Stack(const int& _size)
    {
        m_Capacity  = _size * 2;
        m_Data      = new T[m_Capacity];
    }
    ~Stack() noexcept
    {
        delete[] m_Data;
    }
    int Size() const
    {
        return m_Size;
    }
    int Capacity() const
    {
        return m_Capacity;
    }
    void Push(const T& Item)
    {
        if (m_Size == m_Capacity) {
            this->Relocate();
        }
        m_Data[++m_Top] = Item;
        m_Size++;
    }
    T Pop()
    {
        T Item = this->Top();
        m_Top--;
        m_Size--;
        return Item;
    }
    T Top() const
    {
        if (m_Top < 0) {
            throw std::runtime_error("Attempt to access for empty stack.");
        }
        return m_Data[m_Top];
    }
};

/* Class for Homework 19*/

class Animal
{
public:
    virtual void Voice() 
    {
        cout << "Agrh!\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!\n";
    }
};

class Bird : public Animal
{
public:
    void Voice() override
    {
        cout << "Twit-twit\n";
    }
};

class Cow : public Animal
{
public:
    void Voice() override
    {
        cout << "Moooo~\n";
    }
};

int main()
{
    /* Homework 17
     * 1) Создать класс со своими данными, скрытыми при помощи private. 
     *    Сделать public метод для вывода этих данных. Протестировать. 
     * 2) Дополнить класс Vector public методом, который будет 
    */
    {
        cout << "Homework 17\n";
        Vector vector(2.0, 2.0, 2.0);
        cout << vector.Module() << endl << endl;;
    }
    /* Homework 18
     * 1) Создать класс Stack, который будет реализовывать принцип работы структуры данных стек. 
     *    Размер стека не должен быть ограничен (используйте динамический массив). 
     * 2) Реализовать для любого типа данных на выбор (int, float, double, string). 
     *    Должны быть как минимум методы pop(), чтобы достать из стека верхний элемент, и push(), 
     *    чтобы добавить новый элемент.
     */
    {
        cout << "Homework 18\n";
        try {
            Stack<int> stack;
            cout << "Stacks capacity = " << stack.Capacity()
                 << " & stacks size = " << stack.Size() << endl;

            for (int i = 1; i <= 5; ++i) {
                stack.Push(i);
            }
            cout << "Five items have pushed into stack\n";
            cout << "Stacks capacity = " << stack.Capacity()
                 << " & stacks size = " << stack.Size() << endl;

            cout << "Top item: " << stack.Top() << endl;
            for (int i = 1; i <= 4; ++i) {
                stack.Pop();
            }
            cout << "Four items have popped from stack\n";
            cout << "Stacks capacity = " << stack.Capacity()
                << " & stacks size = " << stack.Size() << endl;

            cout << "Top item: " << stack.Top() << endl;
            stack.Push(9999);
            cout << "Top item: " << stack.Top() << endl;

            cout << "Stacks capacity = " << stack.Capacity()
                 << " & stacks size = " << stack.Size() << endl;
        } 
        catch (std::runtime_error& e) {
            cout << e.what() << endl;
        }
        cout << endl;
    }
    /* Homework 19
     * 1) Создать класс Animal с публичным методом Voice() который выводит в консоль строку с текстом. 
     * 2) Наследовать от Animal минимум три класса (к примеру Dog, Cat и т.д.) и в них перегрузить метод Voice() 
     *     таким образом, чтобы для примера в классе Dog метод Voice() выводил в консоль "Woof!". 
     * 3) В функции main создать массив указателей типа Animal и заполнить этот массив объектами созданных классов. 
     *    Затем пройтись циклом по массиву, вызывая на каждом элементе массива метод Voice(). 
     * 4)Протестировать его работу, должны выводиться сообщения из ваших классов наследников Animal. 
    */
    {
        cout << "Homework 19\n";
        std::vector<Animal*> Animals
        {
            new Dog(), new Cat(), 
            new Bird(), new Cow()
        };

        for (const auto& Item : Animals) {
            Item->Voice();
        }
    }

}

